"""Converts a taxonomy dump file into a sqlite database."""
import argparse
import shutil
from io import StringIO
from tempfile import NamedTemporaryFile
from typing import Dict, List

import pandas as pd
from peewee import SqliteDatabase

from taxonomy_converter import models

temp_db_file = NamedTemporaryFile().name  # pylint: disable=consider-using-with
sqlite_db = SqliteDatabase(temp_db_file)


def cli() -> None:
    """Run the conversion from CLI arguments."""
    parser = argparse.ArgumentParser(
        description="Converts a taxonomy dump file (CSV) into a database (sqlite)"
    )
    parser.add_argument(
        "input_file", metavar="if", type=str, help="The input dump file"
    )
    parser.add_argument("output_file", metavar="of", type=str, help="The output sqlite")
    args = parser.parse_args()

    input_file = args.input_file
    output_file = args.output_file

    run(input_file, output_file)


def run(in_file: str, out_file: str) -> None:
    """Run the conversion process.

    Parameters
    ----------
    in_file : str
        The input dump file.
    out_file : str
        The output file name. By default None. If None, you are expected to initialize the
        db with `models.db.initialize(sqlite_db)`
    """
    if out_file is not None:
        models.db.initialize(sqlite_db)

    init_db()
    df = load_data(in_file)
    types = save_taxonomie_types(df)
    save_taxonomies(df, types)

    if out_file is not None:
        move_database(out_file)


def move_database(out_file: str) -> None:
    """Move the temp database into it's final location.

    Parameters
    ----------
    out_file : str
        The output file location.
    """
    shutil.move(temp_db_file, out_file)


def save_taxonomies(df: pd.DataFrame, types: Dict[str, models.TaxonomyType]) -> None:
    """Save the taxonomy types to the database.

    Parameters
    ----------
    df : pd.DataFrame
        The loaded dataframe containing the taxonomies
    types : Dict[str, models.TaxonomyType]
        A dict of all the available taxonomy types with the key being the string rep in the df.
    """
    i = 0
    batch_size = 1000
    taxonomies: List[models.Taxonomy] = []
    for _, row in df.iterrows():
        t_id = int(str(row["id"]))
        t_type = types[str(row["type"])]
        mostly_empty = row["mostly_empty"]
        if pd.isna(mostly_empty):
            mostly_empty = None

        tax = models.Taxonomy(
            taxonomy_id=t_id,
            description=row["description"],
            mostly_empty=mostly_empty,
            type=t_type,
        )
        taxonomies.append(tax)
        i += 1
        if i >= batch_size:
            with models.db.atomic():
                models.Taxonomy.bulk_create(taxonomies)
            taxonomies = []
            i = 0

    # Dump remaining items
    with models.db.atomic():
        models.Taxonomy.bulk_create(taxonomies)


def save_taxonomie_types(df: pd.DataFrame) -> Dict[str, models.TaxonomyType]:
    """Save the taxonomy types into the database.

    Parameters
    ----------
    df : pd.DataFrame
        The full dataframe containing all the taxonomies and their types.

    Returns
    -------
    Dict[str, models.TaxonomyType]
        A dictionary with the key being the string representation of the type and the Taxonomy object.
    """
    types: Dict[str, models.TaxonomyType] = {}

    item: str
    for item in df["type"].unique():
        t = models.TaxonomyType(name=item)
        t.save()
        types[item] = t
    return types


def load_data(in_file: str) -> pd.DataFrame:
    """Load the data from the input file.

    Parameters
    ----------
    in_file : str
        The input file to be read.

    Returns
    -------
    pd.DataFrame
        A dataframe from the input file.
    """
    with open(in_file, encoding="utf-8") as fh:
        raw_string = fh.read()
    raw_string = raw_string.replace("\t|\t", "|").replace("\t|", "")
    df = pd.read_csv(StringIO(raw_string), sep="|", header=None)

    df.columns = ["id", "description", "mostly_empty", "type"]
    return df


def init_db() -> None:
    """Create a new empty database."""
    models.db.create_tables([models.TaxonomyType, models.Taxonomy])


if __name__ == "__main__":
    INPUT_FILE = "taxonomy_ids.dmp"
    OUTPUT_FILE = "taxonomy.sqlite"

    run(INPUT_FILE, OUTPUT_FILE)
