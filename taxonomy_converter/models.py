"""Taxonomy models."""
from peewee import AutoField, CharField, ForeignKeyField, IntegerField, Model
from peewee import Proxy

db = Proxy()


class TaxonomyType(Model):  # type: ignore
    """Contains a taxonomy type like 'type material', 'in-part', ..."""

    id = AutoField()
    name = CharField(50)

    class Meta:
        """Meta."""

        database = db


class Taxonomy(Model):  # type: ignore
    """The actual taxonomy with the tax_id and description."""

    id = AutoField()
    taxonomy_id = IntegerField()
    description = CharField(50)
    mostly_empty = CharField(50, null=True)
    type = ForeignKeyField(TaxonomyType, backref="types")

    class Meta:
        """Meta."""

        database = db
