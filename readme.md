# Taxonomy converter

[![pipeline status](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/taxonomy-converter/badges/master/pipeline.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/taxonomy-converter/-/commits/master)
[![coverage report](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/taxonomy-converter/badges/master/coverage.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/taxonomy-converter/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

This package converts a taxonomy dump file (CSV) into a database (sqlite). This allows for
faster searches within the data.

## Prerequisites

You will need:

- `python3.9` (see `pyproject.toml` for full version)

Simply run `taxonomy-converter taxonomy_ids_subsample.dmp out.sqlite` to get a sample output.

## Development

When developing locally, we use:

- [`git`](https://git-scm.com/) (**required**)
- [`pre-commit`](https://pre-commit.com/) (**required**)
- [`poetry`](https://github.com/python-poetry/poetry) (**required**)
- [`vscode`](https://code.visualstudio.com/)

### Getting started

- Installing the required packages `poetry install`
- Testing the CLI interface: `poetry run taxonomy-converter taxonomy_ids_subsample.dmp out.sqlite`
- Debugging: Run the default VSCode `run configuration` to get started with debugging

Further documentation is available [here](https://ip34-21vt.pages.fhnw.ch/ip34-21vt_proteomics/proteomics-docs/)

## Usage example

```python
from typing import List
from peewee import SqliteDatabase, fn

from taxonomy_converter.convert import Taxonomy, db

sqlite_db = SqliteDatabase("./taxonomy.sqlite")
db.initialize(sqlite_db)

all_results: List[Taxonomy] = Taxonomy.select(Taxonomy.taxonomy_id).where(fn.Lower(Taxonomy.description) == "homo sapiens")

for result in all_results:
    print("taxonomy_id: ", result.taxonomy_id)
```
